import React from 'react';
import './App.css';
import {useAccount, useConnect, useDisconnect, useEnsName} from 'wagmi'
import $ from 'jquery';

function App() {
    const [amount, setAmount] = React.useState(0);
    const {connect, connectors} = useConnect();

    const {address, isConnected} = useAccount()
    const {data: ensName} = useEnsName({address})
    const {disconnect} = useDisconnect();

    $(document).on('click', '.buy-with-wallet', function () {
        const amount = $('.total-amount').text();
        setAmount(parseFloat(amount));
    });


    return <div>
        {isConnected ? <div>
            Connected to {ensName ?? address}
            <button onClick={() => disconnect()}>Disconnect</button>
        </div> : <div>Not connected</div>}
        {connectors.map(connector => (
            <button onClick={() => connect({connector})}>Connect {connector.name}</button>
        ))}
        <div>Amount: {amount}</div>
    </div>
}

export default App;
