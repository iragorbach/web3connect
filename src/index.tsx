import React from 'react';
import ReactDOM from 'react-dom/client';
import {w3mProvider} from '@web3modal/ethereum';
import './index.css';
import App from './App';
import {configureChains, createConfig, WagmiConfig} from "wagmi";
import {arbitrum, mainnet, polygon} from "viem/chains";

import {CoinbaseWalletConnector} from 'wagmi/connectors/coinbaseWallet';
import {MetaMaskConnector} from 'wagmi/connectors/metaMask';
import {WalletConnectConnector} from 'wagmi/connectors/walletConnect';

const chains = [arbitrum, mainnet, polygon];
const projectId = '4d6a554702edaed6fc0411e9dd9cd5ea';

const {publicClient} = configureChains(chains, [w3mProvider({projectId})]);

const wagmiConfig = createConfig({
    autoConnect: true,
    connectors: [
        new WalletConnectConnector({
            chains,
            options: {
                projectId: projectId
            }
        }),
        new MetaMaskConnector({chains}),
        new CoinbaseWalletConnector({
            chains,
            options: {
                appName: 'wagmi'
            }
        })
    ],
    publicClient
});

const root = ReactDOM.createRoot(
    document.getElementById('web3-container') as HTMLElement
);
root.render(
    <WagmiConfig config={wagmiConfig}>
        <App/>
    </WagmiConfig>
);
